#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { HelloWordLambdaStack } from '../lib/hello-world-stack';

const app = new cdk.App();
new HelloWordLambdaStack(app, 'HelloWorldLambdaStack', {
    env: {
        region: process.env.AWS_DEFAULT_REGION,
        account: process.env.CDK_DEFAULT_ACCOUNT,
    }
});
