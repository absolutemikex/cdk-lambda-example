import cdk = require('@aws-cdk/core');
import lambda = require('@aws-cdk/aws-lambda');

export class HelloWordLambdaStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const lambdaFunction = new lambda.Function(this, 'lambdafunction', {
      code: lambda.Code.fromAsset(__dirname + '../../../src'),
      handler: 'index.handler',
      runtime: lambda.Runtime.NODEJS_12_X,
      functionName: 'hello-world-function'
    })
}
}
